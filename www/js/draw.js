/*** GUI Part ***/
var tool = 0;
var tools = document.querySelectorAll(".tool");
var drawColor = null;
var radius = 10;
var n = [50, 50, 60];
setColor(n);

for (var i = 0; i < tools.length; i++) {
    tools[i].onclick = function (id) {
        return function () {
            setTool(id);
        }
    }(i);
}

function setTool(id) {
    tool = id;
    for (var i = 0; i < tools.length; i++) {
        tools[i].classList.remove("tool-selected");
        if (id == i)
            tools[i].classList.add("tool-selected");
    }
}

function setColor(c) {
    drawColor = c;
    document.querySelector('#colorPreview').style.backgroundColor =
        'rgb(' +
        drawColor[0] + ',' +
        drawColor[1] + ',' +
        drawColor[2] +
        ')';
    document.querySelector('#colorPreview').style.backgroundColor =
        'rgb(' +
        drawColor[0] + ',' +
        drawColor[1] + ',' +
        drawColor[2] +
        ')';
    if ((drawColor[0] + drawColor[1] + drawColor[2]) / 3 < 122)
        document.querySelector('#colorPreview').style.color = 'white';
    else
        document.querySelector('#colorPreview').style.color = 'black';
}

document.querySelector("#saveDraw").onclick = function () {
    app.closePane(document.querySelector("#ok-draw"), function () {
        app.openPane(document.querySelector("#saved"));
    });

    graphic.elt.toBlob(function (blob) {
        saveImage(blob);
    });

    setTimeout(function () {
        app.closePane(document.querySelector("#saved"), function () {
            goToMenu(dibujo);
        });
    }, 3000);

};

document.querySelector("#btn-back").onclick = function () {
    app.openPane(document.querySelector("#exit-confirm"));
};

document.querySelector("#exit-draw").onclick = function () {
    app.closePane(document.querySelector("#exit-confirm"), function(){
        goToMenu(dibujo);
        pauseInterval();
    });
};

document.querySelector("#keep-drawing").onclick = function () {
    app.closePane(document.querySelector("#exit-confirm"));
};


document.querySelector("#color-selector").onclick = function () {
    app.openPane(document.querySelector("#color-picker"));
};

document.querySelector("#close-color-picker").onclick = function () {
    app.closePane(document.querySelector("#color-picker"));
};

for (var range of document.querySelectorAll(".color-range")) {
    range.oninput = refreshColorPicker;
}

function refreshColorPicker() {
    n = [0, 0, 0];
    for (var i = 0; i < document.querySelectorAll(".color-range").length; i++) {
        n[i] = parseInt(document.querySelectorAll(".color-range")[i].value);
    }
    setColor(n);
}

function goToMenu(screen){
    app.changeScreen(screen, splash, true, function(){
        screen.classList.add('hidden');
        splash.classList.remove('hidden');
    }, function(){
        app.showMenu();
        graphic.clear();
        graphic.background(255);
    });
}

/*** Drawing part ***/
var lastPoint = null;
var graphic = null;


function setup() {
    createCanvas(300, 300).parent("#canvas");
    ellipseMode(CENTER);
    graphic = createGraphics(300, 300);
    graphic.background(255);
}

function draw() {
    background(255);
    image(graphic, 0, 0);

    if (tool == 0 || tool == 1)
        tool0Preview();

    if (mouseIsPressed) {
        if (!document.querySelector("#color-picker").classList.contains("open")
            && !document.querySelector("#ok-draw").classList.contains("open")
            && !document.querySelector("#share").classList.contains("open")
            && !document.querySelector("#saved").classList.contains("open")
            && !document.querySelector("#exit-confirm").classList.contains("open"))
            drawOnGraphic();
    } else {
        lastPoint = null;
        // onMouseQuit()
    }

}

function tool0Preview() {
    noFill();
    stroke(200);
    strokeWeight(2);
    ellipse(mouseX, mouseY, radius, radius);
}

function drawOnGraphic() {
    if (lastPoint == null)
        lastPoint = [mouseX, mouseY];

    if (tool == 0) {
        graphic.noFill();
        graphic.stroke(drawColor);
        graphic.strokeWeight(radius);
        graphic.line(mouseX, mouseY, lastPoint[0], lastPoint[1]);
    }

    if (tool == 1) {
        graphic.noFill();
        graphic.stroke(255);
        graphic.strokeWeight(radius);
        graphic.line(mouseX, mouseY, lastPoint[0], lastPoint[1]);
    }

    lastPoint = [mouseX, mouseY];
}
