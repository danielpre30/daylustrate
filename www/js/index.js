/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * 'License'); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * 'AS IS' BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
"use strict";

var btnIniciar, wrapperIniciar, title, logo, homeButtonsCont,
    btnDibujoDelDia, splash, dibujo, btnOkDraw, modalBox,
    btnShare, paneShare, btnRealizados, realizadosScreen,
    btnBackDone, thumbnailsCont, dibujoScreen, btnBackView,
    btnRecords, recordsCont, recordScreen, btnBackRecord,
    btnTrash;

var storage;
var drawList;
var currentDraw;
var interval;
var seconds;
var lastScreen;

var app = {
    // Application Constructor
    initialize: function () {
        this.loadElements();
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
        // this.onDeviceReady();
    },
    loadElements: function () {
        wrapperIniciar = document.getElementById('wrapper-iniciar');
        btnIniciar = document.getElementById('btn-iniciar');
        title = document.getElementById('app-name');
        logo = document.getElementById('logo');
        homeButtonsCont = document.getElementById('home-buttons-cont');
        btnDibujoDelDia = document.getElementById('btn-dibujo-del-dia');
        splash = document.getElementById('splash');
        dibujo = document.getElementById('dibujo');
        btnOkDraw = document.getElementById('btn-ok-draw');
        modalBox = document.getElementById('configuration');
        btnShare = document.getElementById('btn-share');
        paneShare = document.getElementById('share');
        btnRealizados = document.getElementById('btn-dibujos-realizados');
        realizadosScreen = document.getElementById('realizados');
        btnBackDone = document.getElementById('btn-back-done');
        thumbnailsCont = document.getElementById('thumbnails-cont');
        dibujoScreen = document.getElementById('ver-dibujo');
        btnBackView = document.getElementById('btn-back-view');
        btnRecords = document.getElementById('btn-records');
        recordsCont = document.getElementById('records-cont');
        recordScreen = document.getElementById('records');
        btnBackRecord = document.getElementById('btn-back-record');
        btnTrash = document.getElementById('btn-trash');
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        this.loadEvents();

        animateCSS(logo, 'slideInDown', null);
        animateCSS(title, 'slideInUp', null);
        setTimeout(this.showBtnIniciar, 2300);
        this.initializeDraws();
    },

    loadEvents: function () {
        btnIniciar.addEventListener('click', function () {
            animateCSS(wrapperIniciar, 'fadeOut', function () {
                wrapperIniciar.classList.add('hidden');
                this.showMenu();
            }.bind(this));
        }.bind(this));
        btnDibujoDelDia.addEventListener('click', this.showDibujo.bind(this));
        btnOkDraw.addEventListener('click', function () {
            this.openPane(document.querySelector("#ok-draw"));
        }.bind(this));
        btnShare.addEventListener('click', function () {
            this.closePane(document.querySelector("#ok-draw"), function () {
                this.openPane(paneShare);
            }.bind(this));
        }.bind(this));
        btnRealizados.addEventListener('click', function () {
            while (thumbnailsCont.firstChild) {
                thumbnailsCont.removeChild(thumbnailsCont.firstChild);
            }
            animateCSS(splash, 'fadeOutLeft', function () {
                splash.classList.add('hidden');
                homeButtonsCont.classList.add('hidden');
                realizadosScreen.classList.remove('hidden');
                animateCSS(realizadosScreen, 'fadeInRight');
            });

            let doneDraws = drawList.filter(function (current, index) {
                return current.status == 'Hecho';
            });
            let lastRow;

            doneDraws.forEach(element => {
                let image = document.createElement('img');
                image.className = 'thumbnail';
                image.src = element.url;
                image.name = element.name;
                image.addEventListener('click', function () {
                    app.viewDraw(realizadosScreen, element);
                });
                if (lastRow) {
                    if (lastRow.childElementCount >= 3) {
                        lastRow = document.createElement('div');
                        lastRow.className = 'thumbnail-row';
                        thumbnailsCont.appendChild(lastRow);
                    }
                }
                else {
                    lastRow = document.createElement('div');
                    lastRow.className = 'thumbnail-row';
                    thumbnailsCont.appendChild(lastRow);
                }

                lastRow.appendChild(image);
            });

        });
        modalBox.addEventListener('click', this.closeModalBox.bind(this));
        btnBackDone.addEventListener('click', function () {
            goToMenu(realizadosScreen);
        });
        btnBackView.addEventListener('click', function () {
            if (lastScreen == realizadosScreen) {
                app.changeScreen(dibujoScreen, realizadosScreen, true, function () {
                    dibujoScreen.classList.add('hidden');
                    realizadosScreen.classList.remove('hidden');
                });
            }
            else if (lastScreen == recordScreen) {
                app.changeScreen(dibujoScreen, recordScreen, true, function () {
                    dibujoScreen.classList.add('hidden');
                    recordScreen.classList.remove('hidden');
                });
            }

        });
        btnBackRecord.addEventListener('click', function () {
            goToMenu(recordScreen);
        });
        btnRecords.addEventListener('click', function () {
            while (recordsCont.firstChild) {
                recordsCont.removeChild(recordsCont.firstChild);
            }
            this.changeScreen(splash, recordScreen, false, function () {
                splash.classList.add('hidden');
                recordScreen.classList.remove('hidden');
            });

            let doneDraws = drawList.filter(function (current) {
                return current.status == 'Hecho';
            });
            doneDraws.forEach(element => {
                let h1 = document.createElement('h1');
                h1.className = 'record';
                h1.textContent = element.time;
                h1.addEventListener('click', function () {
                    app.viewDraw(recordScreen, element);
                });
                recordsCont.appendChild(h1);
            });
        }.bind(this));
    },

    initializeDraws: function () {
        storage = window.localStorage;

        const tempStorage = storage.getItem('drawList');

        if (tempStorage) {
            drawList = JSON.parse(tempStorage);
        }
        else {
            const newDraws =
                [
                    { id: 0, name: 'León', status: 'Nuevo', url: '' },
                    { id: 1, name: 'Silla', status: 'Nuevo', url: '' },
                    { id: 2, name: 'Teléfono', status: 'Nuevo', url: '' },
                    { id: 3, name: 'Familia', status: 'Nuevo', url: '' },
                    { id: 4, name: 'Avión', status: 'Nuevo', url: '' },
                    { id: 5, name: 'Casa', status: 'Nuevo', url: '' },
                    { id: 6, name: 'Carro', status: 'Nuevo', url: '' }
                ];
            storage.setItem('drawList', JSON.stringify(newDraws));
            drawList = newDraws;
        }
    },

    getRandomDraw: function () {
        let availableDraws = drawList.filter(function (current) {
            return current.status === 'Nuevo';
        });
        return availableDraws[Math.floor(Math.random() * availableDraws.length)];
    },

    // Update DOM on a Received Event
    showBtnIniciar: function () {
        wrapperIniciar.classList.remove('hidden');
        animateCSS(wrapperIniciar, 'fadeIn', null, 'slow');
    },

    showMenu: function () {
        title.classList.add('slideDownTitle');
        logo.classList.add('slideUpLogo');

        setTimeout(function () {
            homeButtonsCont.classList.remove('hidden');
            animateCSS(homeButtonsCont, 'fadeIn', null);
        }.bind(this), 2000);
    },

    showDibujo: function () {
        currentDraw = this.getRandomDraw();
        document.querySelector('#drawTitle').textContent = currentDraw.name;
        animateCSS(splash, 'fadeOutLeft', function () {
            splash.classList.add('hidden');
            homeButtonsCont.classList.add('hidden');
            dibujo.classList.remove('hidden');
            animateCSS(dibujo, 'fadeInRight');
        });
        startInterval();
    },
    closeModalBox: function (ev) {
        if (ev.target.id === 'configuration') {
            this.closePane(document.querySelector(".pane.open"));
        }
    },

    closePane: function (pane, callback) {
        pane.classList.remove('open', 'bounceIn');
        animateCSS(pane, 'bounceOut', function () {
            modalBox.classList.add('hidden');
            pane.classList.add('hidden');
            if (typeof callback === 'function') callback();
        });
    },

    openPane: function (pane) {
        modalBox.classList.remove('hidden');
        pane.classList.remove('hidden');
        pane.classList.add('open');
        animateCSS(pane, 'bounceIn');
    },
    viewDraw: function (fromScreen, draw) {
        lastScreen = fromScreen;
        btnTrash.onclick = function () {
            drawList = drawList.map(function (element, index) {
                if (element.id == draw.id) {
                    element.status = 'Nuevo';
                }
                return element
            });
            storage.setItem('drawList', JSON.stringify(drawList));
        };
        this.changeScreen(fromScreen, dibujoScreen, false, function () {
            fromScreen.classList.add('hidden');
            dibujoScreen.classList.remove('hidden');
        });
        document.querySelector('#drawTitle-view').textContent = draw.name;
        document.querySelector('#image-view').src = draw.url;
    },
    changeScreen: function (screenA, screenB, reverse, callbackA, callbackB) {
        if (reverse) {
            animateCSS(screenA, 'fadeOutRight', callbackA);
            animateCSS(screenB, 'fadeInLeft', callbackB);
        }
        else {
            animateCSS(screenA, 'fadeOutLeft', callbackA);
            animateCSS(screenB, 'fadeInRight', callbackB);
        }

    }
};

app.initialize();

function writeFiles(fileEntry, dataObj) {
    // Create a FileWriter object for our FileEntry (log.txt).
    fileEntry.createWriter(function (fileWriter) {

        fileWriter.onwriteend = function () {
            console.log('Successful file write...');
            // readFile(fileEntry);
        };

        fileWriter.onerror = function (e) {
            console.log('Failed file write: ' + e.toString());
        };

        // If data object is not passed in,
        // create a new Blob instead.
        if (!dataObj) {
            dataObj = new Blob(['some file data'], { type: 'image/png' });
        }

        fileWriter.write(dataObj);
    });
}
function readFile(fileEntry) {

    fileEntry.file(function (file) {
        var reader = new FileReader();

        reader.onloadend = function () {
            console.log('Successful file read');
        };

        reader.readAsText(file);

    }, function () {
        console.log('Error while file reading');
    });
}
function saveImage(image) {
    pauseInterval();
    const fileName = currentDraw.id + '.' + currentDraw.name + '.png';
    const path = cordova.file.externalApplicationStorageDirectory;
    window.resolveLocalFileSystemURL(path, function (fs) {
        fs.getDirectory('images', { create: true }, function (subDirEntry) {
            subDirEntry.getFile(fileName, { create: true, exclusive: false }, function (fileEntry) {
                writeFiles(fileEntry, image);
                drawList = drawList.map(function (element, index) {
                    if (element.id == currentDraw.id) {
                        element.url = path + 'images/' + fileName;
                        element.status = 'Hecho';
                        let division = seconds / 60;
                        let minutos = Math.floor(division);
                        let segundos = (division - minutos) * 60;
                        element.time = minutos + ' : ' + segundos;
                    }
                    return element
                });
                storage.setItem('drawList', JSON.stringify(drawList));
            }, function () {
                console.log('Create file fail...');
            })
        });

    }, function () {
        console.log('File system fail...')
    });
}

function animateCSS(node, animationName, callback, speed) {
    if (speed) {
        node.classList.add('animated', animationName, speed);
    }
    else {
        node.classList.add('animated', animationName);
    }

    function handleAnimationEnd() {
        node.classList.remove('animated', animationName);
        node.removeEventListener('animationend', handleAnimationEnd);

        if (typeof callback === 'function') callback();
    }

    node.addEventListener('animationend', handleAnimationEnd);
}

function pauseInterval() {
    clearInterval(interval);
}

function startInterval() {
    seconds = 0;
    interval = setInterval(function () {
        seconds++;
    }, 1000);
}